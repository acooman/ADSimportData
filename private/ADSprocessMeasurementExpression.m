function res = ADSprocessMeasurementExpression(simdata)
%ADSPROCESSMEASUREMENTEXPRESSION Loads measurement expression from an ADS matlab file

f = fields(simdata);
dataBlocks = simdata.(f{1}).dataBlocks;

% get the data out of the ADS structure
for ii=1:length(dataBlocks)
    if strcmpi(dataBlocks(ii).type,'MeasurementExpression')
        % get the name of the dependents
        dep = dataBlocks(ii).dependents;
        if length(dep)==1
            % generate a variable name out of the dependent string (gets rid of possible '.' out of the name)
            dep = genvarnamemeasexp(dep{1});
        else
            error('I havent implemented measurement expressions with complicated names yet')
        end
        res.(dep) = ADSprocessDataBlock(dataBlocks(ii));
    end
end

end

function res = genvarnamemeasexp(str)
% replace all '.' by an underscore
res = regexprep(str,'\.','_');
end
