function tran=ADSprocessTRANdata(filename)
% this function loads trainsient simulation data from a mat file generated by ADS.
%
%   tran = ADSprocessTRANdata(filename)
%
%   filename is the string that contains the location of the .mat file
%   generated by ADS.
%   
%   tran is a struct which contains has the different saved nodes as fields.
%   if the saved signal is a current, it will be saved as I_nodename
%
% V1.0 Adam Cooman 4/13

% get the data out using the ADSgetData function
[tran,simdata]=ADSgetData(filename,'Transient');

% add the frequency vector to the dataset
tran.time = simdata.data(1).independent;

% remove the tranorder field, because nobody uses it anyway
tran = rmfield(tran,'tranorder');

end