function res2 = ADSresolveMatricesAndStructs(input)
% ADSparseResultStruct cleans up a result struct structs and matrices are extracted
%
%   res = ADSparseResultStruct(struct)
%
% struct is a structure loaded internally in ADSimportSimData
%
%   It contains fields of the type name[x,..,z] which indicate a matrix
%   if those fields are found, extract the corresponding matrix nicely
%
%   The structure can also contain fields of the type name.name, which
%   indicates a struct, in that case, the struct has to be extracted
%
% Adam Cooman, ELEC VUB

res=struct();

fields = fieldnames(input);

% look for matrices in the fieldnames. Matrix indexing in ADS uses [,].
% When this is passed through genvarname, we obtain
% genvarname('[') gives 0x5B
% genvarname(']') gives 0x5D
% genvarname(',') gives 0x2C
mat = regexp(fields,'(?<matName>[a-zA-Z]+)0x5B(?<inds>\d+(0x2C\d+)*)0x5D$','names');
info=struct();
for ii=1:length(mat)
    if ~isempty(mat{ii})
        if ~isfield(info,mat{ii}.matName);
            info.(mat{ii}.matName).foundInds = cellfun(@str2double,regexp(mat{ii}.inds,'0x2C','split'));
        else
            info.(mat{ii}.matName).foundInds(end+1,:) = cellfun(@str2double,regexp(mat{ii}.inds,'0x2C','split'));
        end
    else
        % no matrix index is found. just pass this field on into the result struct
        res.(fields{ii}) = input.(fields{ii});
    end
end

% now just look at the maximum of each dimension to in the found indices to
% determine the actual size of the matrices in the original struct
matNames = fieldnames(info);
for mm=1:length(matNames)
    info.(matNames{mm}).size = max(info.(matNames{mm}).foundInds,[],1);
end

% fill the matrices now in a decent way
for mm=1:length(matNames)
     % generate the linear indexing vector of the matrix that is being processed
     lin = 1:prod(info.(matNames{mm}).size);
     inds = cell(length(info.(matNames{mm}).size),1);
     for ii=1:length(lin);
         % generate the actual indices
         [inds{:}] = ind2sub(info.(matNames{mm}).size,lin(ii));
         % generate the field name
         field = [matNames{mm} generateADSinds([inds{:}])];
         if ii==1
             % get the size of the matrix. This is an S1 x S2 x ... x Sn x F
             % where S is a sweep
             siz = size(input.(field));
             % determine the amount of sweeps
             if length(siz)==2
                 if siz(1)==1; numSweeps=0;else numSweeps=1;end
             else
                numSweeps = length(siz)-1;
             end
             % preallocate the resulting matrix. This one is a matrix of size
             % S1 x S2 x ... x Sn x P1 x P2 x ... x Pm x F  
             % where S is a sweep, P is a dimension of the matrix 
             % F is the amount of frequencies
             res.(matNames{mm}) = zeros([siz(1:numSweeps) info.(matNames{mm}).size siz(end)]);
         end
         
         % now call the subsasgn function to assing the vector to the matrix
         %   A = subsasgn(A, S, B) is called by MATLAB for the syntax A(i) = B
         %   S is a struct array with two fields, type and subs.
         %   type is a string containing '()'
         %   subs is a cell array or string containing the actual subscripts.
         S.type='()';
         S.subs=[repmat({':'},[1 numSweeps]) inds{:} {':'}];
         res.(matNames{mm}) = subsasgn(res.(matNames{mm}), S , input.(field));
     end
end

% clear all variables exept res
clearvars('-except','res');

% look for possible structs in the obtained structures
fields = fieldnames(res);
for ff=1:length(fields)
    % replace all the genvarnamed dots by real dots (genvarname('.') = '0x2E')
    newFields = regexprep(fields{ff}, '0x2E', '.');
    % now just use eval to assign the new struct
    eval(['res2.' newFields '=res.(fields{ff});']);
end


end

function res = generateADSinds(inds)
    str = ['[' num2str(inds(1))];
    for ii=2:length(inds)
        str = [str ',' num2str(inds(ii))];
    end
    str = [str ']'];
    res=genvarname(str);
    % genvarname adds an x before, so remove that
    res = res(2:end);
end